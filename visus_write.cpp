/***************************************************
** ViSUS Visualization Project                    **
** Copyright (c) 2010 University of Utah          **
** Scientific Computing and Imaging Institute     **
** 72 S Central Campus Drive, Room 3750           **
** Salt Lake City, UT 84112                       **
**                                                **
** For information about this project see:        **
** http://www.pascucci.org/visus/                 **
**                                                **
**      or contact: pascucci@sci.utah.edu         **
**                                                **
****************************************************/

#include <Visus/IdxDataset.h>
#include <iostream>
#include <ctime>

using namespace Visus;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//TUTORIAL_1 create a dataset of dimension (16,16,16) and write some data
//////////////////////////////////////////////////////////////////////////////////////////////////////////
int main()
{
  //each sample is made of one uint32 (so a sample has 1*32 bits=32 bits)
  //each atomic data is a 32 bit unsigned integer
  //valid dtypes are in the format [u](int|float)nbits
  //I want to write data on disk compressed using zlib
  //possible flag values are [compressed]  (space separated!)
  //IMPORTANT you can have a multifield file if you want. For example
  //"MY_SCALAR_FIELD 1*uint8 compressed + MY_VECTORIAL_FIELD 3*float32 "
  //in this case, when you read or write, specify the field you want Dataset::field/Dataset::field
  IdxModule::attach();

  String filename="./tutorial_1.idx";
  String default_layout="1";

  //the data will be in the bounding box  p1(0,0,0) p2(16,16,16) (p1 included, p2 excluded)
  IdxFile idxfile;
  idxfile.box=NdBox(NdPoint(0, 0, 0), NdPoint::one(512, 512, 512));
  {
    Field field("myfield", DTypes::UINT32);
    field.default_compression="";
    field.default_layout=default_layout;
    idxfile.fields.push_back(field);
    //idxfile.bitsperblock = 24;
  }
  VisusReleaseAssert(idxfile.save(filename));

  //now create a Dataset, save it and reopen from disk
  auto dataset=Dataset::loadDataset(String("./tutorial_1.idx"));
  VisusReleaseAssert(dataset && dataset->valid());

  //any time you need to read/write data from/to a Dataset I need a Access
  auto access=dataset->createAccess();

  //for example I want to write data by slices
  int cont=0;
  float elapsed = 0;
  int x = idxfile.box.p2.get(0);
  int y = idxfile.box.p2.get(1);
  int z = idxfile.box.p2.get(2);
  int num_slices = 16;
  int step = z / num_slices;
  for (int nslice=0; nslice<z; nslice += step)
  {
    //this is the bounding box of the region I'm going to write
    NdBox slice_box=dataset->getBox().getZSlab(nslice, nslice+step);

    //prepare the write query
    auto query=std::make_shared<Query>(dataset.get(), 'w');
    query->position=slice_box;
    VisusReleaseAssert(dataset->beginQuery(query));
    VisusReleaseAssert(query->nsamples.innerProduct()==x*y*step);

    //fill the buffers
    Array buffer(query->nsamples, query->field.dtype);
    unsigned int* Dst=(unsigned int*)buffer.c_ptr();
    for (int I=0; I<x*y*step; I++) *Dst++=cont++;
    query->buffer=buffer;

    //execute the writing
    auto begin = clock();
    VisusReleaseAssert(dataset->executeQuery(access, query));
    auto end = clock();
    elapsed += (end - begin) / (float)CLOCKS_PER_SEC;
  }
  std::cout << "Elapsed time = " << elapsed << "s\n";
  IdxModule::detach();
}
