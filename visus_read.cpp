
/***************************************************
** ViSUS Visualization Project                    **
** Copyright (c) 2010 University of Utah          **
** Scientific Computing and Imaging Institute     **
** 72 S Central Campus Drive, Room 3750           **
** Salt Lake City, UT 84112                       **
**                                                **
** For information about this project see:        **
** http://www.pascucci.org/visus/                 **
**                                                **
**      or contact: pascucci@sci.utah.edu         **
**                                                **
****************************************************/

#include <Visus/IdxDataset.h>
#include <ctime>

using namespace Visus;


////////////////////////////////////////////////////////////////////////
//read data from tutorial 1
////////////////////////////////////////////////////////////////////////
int main()
{
  auto begin = clock();
  IdxModule::attach();
  //read Dataset from tutorial 1
  auto dataset=Dataset::loadDataset("./tutorial_1.idx");
  VisusReleaseAssert(dataset);

  NdBox world_box=dataset->getBox();

  int pdim = 3;

  //check the data has dimension (16,16,16)
  VisusReleaseAssert(dataset->getDefaultField().dtype==(DTypes::UINT32)
    &&  world_box.p1==NdPoint(0, 0, 0)
    &&  world_box.p2==NdPoint::one(256, 256, 256));

  //any time you need to read/write data from/to a Dataset I need a Access
  auto access=dataset->createAccess();

  int x = world_box.p2.get(0);
  int y = world_box.p2.get(1);
  int z = world_box.p2.get(2);
  int num_slices = 256;
  int step = z / num_slices;
  int cont=0;
  float elapsed = 0;
  for (int nslice=0; nslice<z; nslice += step)
  {
    //this is the bounding box of the region I want to read (i.e. a single slice)
    NdBox slice_box=world_box.getZSlab(nslice, nslice+step);

    //I should get a number of samples equals to the number of samples written in tutorial 1
    auto query=std::make_shared<Query>(dataset.get(), 'r');
    query->position=slice_box;
    VisusReleaseAssert(dataset->beginQuery(query));
    VisusReleaseAssert(query->nsamples.innerProduct()==x*y*step);

    //read data from disk
    VisusReleaseAssert(dataset->executeQuery(access, query));
    VisusReleaseAssert(query->buffer.c_size()==sizeof(int)*x*y*step);

    unsigned int* Src=(unsigned int*)query->buffer.c_ptr();
    //for (int I=0; I<x*y*step; I++, cont++)
    //{
    //  VisusReleaseAssert(Src[I]==cont);
    //}
  }
  IdxModule::detach();
  auto end = clock();
  elapsed += (end - begin) / (float)CLOCKS_PER_SEC;
  std::cout << "Elapsed time = " << elapsed << "s\n";
}

